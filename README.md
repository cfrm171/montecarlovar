# MonteCarloVar

. Although VaR measure is objective and intuitive, it doesn’t capture tail risk. There are three commonly used methodologies to calculate VaR – parametric, historical simulation and Monte Carlo simulation. This presentation focuses on Monte Carlo VaR